package org.hung.oauth2.web;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DummyResController {
	
	@RequestMapping(value="/api/echo/{id}")
	public String echo(@PathVariable String id) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return "echo <<"+id+">>" + auth;
	}

}
