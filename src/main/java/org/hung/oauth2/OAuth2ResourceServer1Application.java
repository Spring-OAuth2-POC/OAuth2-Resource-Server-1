package org.hung.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OAuth2ResourceServer1Application {

	public static void main(String[] args) {
		SpringApplication.run(OAuth2ResourceServer1Application.class, args);
	}
}
