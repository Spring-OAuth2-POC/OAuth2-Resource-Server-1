package org.hung.oauth2.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

@Configuration
@EnableResourceServer
public class OAuth2ResourceConfig extends ResourceServerConfigurerAdapter {

	@Autowired
	private ResourceServerTokenServices tokenService;
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		// @formatter:off
		http
		  .requestMatchers()
		    .antMatchers("/api/**")
		  .and().authorizeRequests()
		    .antMatchers("/api/**").access("#oauth2.hasScope('read')");
		//.and()
		//  .
		// @formatter:on
	}
	
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		// @formatter:off
		resources.resourceId("dummy").tokenServices(tokenService);
		// @formatter:on
	}

	@Bean
	public RemoteTokenServices tokenService() {
	    RemoteTokenServices tokenService = new RemoteTokenServices();
	    tokenService.setCheckTokenEndpointUrl("http://localhost:7070/oauth/check_token");
	    tokenService.setClientId("client2");
	    tokenService.setClientSecret("abcd1234");
	    return tokenService;		
	}

	
}
